# AbcSubmit Form Builder

Das **AbcSubmit Forem Builder** ermöglicht die Einbindung von Google Maps im Webshop über den Shop Builder. 


##  AbcSubmit Form Builder einbinden

Öffnen Sie das Menü **CMS » Shop Builder**. Wählen Sie die Seite, auf der Sie Google Maps einbinden wollen. Wählen Sie aus der Liste der Widgets auf der linken Seite das Google Maps Widget und ziehen Sie es via Drag&Drop an die gewünschte Stelle.

Geben Sie im Feld **Google API-Schlüssel** den API-Schlüssel von Google Maps ein. Diesen erhalten Sie von [Google Cloud](https://cloud.google.com/maps-platform/maps/?hl=de).

Geben Sie im Feld **Adresse** die Adresse, die Sie anzeigen lassen wollen, ein.


## Lizenz

Das gesamte Projekt unterliegt der GNU AFFERO GENERAL PUBLIC LICENSE – weitere Informationen finden Sie in der [LICENSE.md](https://github.com/plentymarkets/plugin-ceres/blob/stable/LICENSE.md).

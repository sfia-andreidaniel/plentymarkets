# AbcSubmit Form Builder

The **AbcSubmit Form Builder** enables the integration of Google Maps in your online store via the Shop Builder.

##  Setting up the AbcSubmit Form Builder 

Go to the **CMS » Shop Builder** menu. Select the page into which you want to integrate a form. Select the **AbcSubmit Form Builder** from the list of widgets on the left side of the interface and place it at the desired location via drag & drop.

Click on the **Edit Form** button inside your page to open the form editor.

Create your desired form and click on Publish to save your form inside your page.

## License

This project is licensed under the GNU AFFERO GENERAL PUBLIC LICENSE. – find further information in the [LICENSE.md](https://github.com/plentymarkets/plugin-ceres/blob/stable/LICENSE.md).
